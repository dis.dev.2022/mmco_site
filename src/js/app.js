"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import Choices from 'choices.js'; // Select plugin
import SimpleBar from 'simplebar'; // Кастомный скролл
import HystModal from 'hystmodal'; // Modal
import {WebpMachine} from "webp-hero"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();


// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Modal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
    catchFocus: false
});

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки

if (document.querySelectorAll('[data-simplebar]').length) {
    document.querySelectorAll('[data-simplebar]').forEach(scrollBlock => {
        new SimpleBar(scrollBlock, {
            autoHide: false
        });
    });
}


// Custom Select
document.querySelectorAll('.pretty-select').forEach(el => {
    const prettySelect = new Choices(el,{
        allowHTML: true,
        searchEnabled: false
    });
});
